using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.TextCore.Text;

public class CreatureController : MonoBehaviour
{
    private List<List<NavMeshAgent>> _navMeshAgentList = new();
    private List<List<Animator>> _animatorList = new();
    [SerializeField]
    private Camera _subCamera;
    [SerializeField]
    private CreatureSelection _creatureSelection;
    void Start()
    {
    }
    private Vector3 _targetPosition;
    private bool _isMoving = false;
    public static bool _isSelect = false;
    private bool _isStop = false;
    void Update()
    {
        AutoSelectCreature();
        SetTarget();
        OldCreatureMoveStop();
        if (_isMoving)
        {
            CreatureLookAt();
            CreatureMoveTo();
        }
    }

    private void AutoSelectCreature()
    {
        if (_isSelect && _creatureSelection._selectedCharacters.Count > 0)
        {
            if (_navMeshAgentList.Count >= 1 && _animatorList.Count >= 1)
            {
                _isMoving = false;
                _navMeshAgentList.Add(_navMeshAgentList[0]);
                _animatorList.Add(_animatorList[0]);
            }

            if (_navMeshAgentList.Count <= 0 && _animatorList.Count <= 0)
            {
                _navMeshAgentList.Add(_creatureSelection._selectedCharacters.Select(character => character.GetComponent<NavMeshAgent>()).ToList());
                _animatorList.Add(_creatureSelection._selectedCharacters.Select(character => character.GetComponent<Animator>()).ToList());
            }
            else
            {
                _navMeshAgentList[0] = _creatureSelection._selectedCharacters.Select(character => character.GetComponent<NavMeshAgent>()).ToList();
                _animatorList[0] = _creatureSelection._selectedCharacters.Select(character => character.GetComponent<Animator>()).ToList();
            }
            _isSelect = false;
        }
    }

    private void OldCreatureMoveStop()
    {
        if (_navMeshAgentList.Count > 1 && _animatorList.Count > 1)
        {
            foreach ((List<NavMeshAgent> navMeshAgent, List<Animator> animator) in _navMeshAgentList.Zip(_animatorList, (agent, anim) => (agent, anim)))
            {
                foreach ((NavMeshAgent navMeshAgent2, Animator animator2) in navMeshAgent.Zip(animator, (agent, anim) => (agent, anim)))
                {
                    if ((animator2.GetCurrentAnimatorStateInfo(0).IsName("Walk") && navMeshAgent2.desiredVelocity == Vector3.zero) || _isStop)
                    {
                        _isStop = true;
                        navMeshAgent2.ResetPath();
                        animator2.SetBool("IsWalk", false);
                    }
                }
                if (_isStop)
                {
                    _navMeshAgentList.Remove(navMeshAgent);
                    _animatorList.Remove(animator);
                    _isStop = false;
                    break;
                }
            }
            
            
        }
    }

    private void SetTarget()
    {
        if (Input.GetMouseButtonDown(1) && _creatureSelection._selectedCharacters.Count > 0)
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = _subCamera.farClipPlane;
            Ray ray = _subCamera.ScreenPointToRay(mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                foreach (NavMeshAgent navMeshAgent in _navMeshAgentList[0])
                {
                    _targetPosition = new Vector3(hit.point.x, navMeshAgent.transform.position.y, hit.point.z);
                }
                _isMoving = true;
            }
        }
    }

    public float animatorSpeedMultiplier = 0.2f;
    private void CreatureMoveTo()
    {

        foreach ((NavMeshAgent navMeshAgent, Animator animator) in _navMeshAgentList[0].Zip(_animatorList[0], (agent, anim) => (agent, anim)))
        {
            Vector3 moveDirection = _targetPosition - navMeshAgent.transform.position;
            float distanceToTarget = moveDirection.sqrMagnitude;
            if (distanceToTarget >= navMeshAgent.stoppingDistance)
            {
                navMeshAgent.destination = _targetPosition;
                float currentWalkSpeed = navMeshAgent.desiredVelocity.magnitude;
                animator.SetFloat("WalkSpeed", currentWalkSpeed * animatorSpeedMultiplier);
                animator.SetBool("IsWalk", true);
            }
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") && navMeshAgent.desiredVelocity == Vector3.zero)
            {
                CreatureMoveStop();
                _isMoving = false;
                return;
            }

        }
    }

    private void CreatureMoveStop()
    {
        foreach ((NavMeshAgent navMeshAgent, Animator animator) in _navMeshAgentList[0].Zip(_animatorList[0], (agent, anim) => (agent, anim)))
        {
            navMeshAgent.ResetPath();
            animator.SetBool("IsWalk", false);
        }
    }
    private float _rotateSpeed = 10f;
    private void CreatureLookAt()
    {
        foreach (NavMeshAgent navMeshAgent in _navMeshAgentList[0])
        {
            if (navMeshAgent.desiredVelocity.normalized != Vector3.zero)
            {
                Quaternion newRotation = Quaternion.LookRotation(navMeshAgent.desiredVelocity.normalized);
                navMeshAgent.transform.rotation = Quaternion.Slerp(navMeshAgent.transform.rotation, newRotation, Time.deltaTime * _rotateSpeed);
            }
        }
    }
}
