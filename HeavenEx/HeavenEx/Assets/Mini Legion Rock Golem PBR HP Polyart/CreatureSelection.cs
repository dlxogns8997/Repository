using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CreatureSelection : MonoBehaviour
{
    public List<GameObject> _selectedCharacters = new List<GameObject>();
    private Vector3 _dragStartPosition;
    private bool _isDragging = false;

    [SerializeField]
    private Camera _subCamera;

    private void Update()
    {

        if (Input.GetMouseButtonDown(0) && _isDragging == false)
        {
            _dragStartPosition = Input.mousePosition;
            _dragStartPosition.z = _subCamera.farClipPlane;
            _selectedCharacters.Clear();
            _isDragging = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            DragSelection();
            HandleSelection();
            _dragStartPosition = Vector3.zero;
            _isDragging = false;
            CreatureController._isSelect = true;
        }

        if (_isDragging)
        {
        }
    }


    private void OnDrawGizmos()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(dragBounds.center, dragBounds.size);

    }
    Bounds dragBounds;
    private void DragSelection()
    {
        GameObject[] characters = FindObjectsOfType<GameObject>().Where(character => character.CompareTag("Creature")).ToArray();

        Vector3 currentMousePosition = Input.mousePosition;
        currentMousePosition.z = _subCamera.farClipPlane;
        Ray startRay = _subCamera.ScreenPointToRay(_dragStartPosition);
        Ray endRay = _subCamera.ScreenPointToRay(currentMousePosition);

        RaycastHit hitStart, hitEnd;
        if (Physics.Raycast(startRay, out hitStart, currentMousePosition.z) && Physics.Raycast(endRay, out hitEnd, currentMousePosition.z))
        {
            Vector3 startPoint = hitStart.point;
            Vector3 endPoint = hitEnd.point;
            dragBounds.SetMinMax(Vector3.Min(startPoint, endPoint), Vector3.Max(startPoint, endPoint));
            Vector3 currentSize = dragBounds.size;
            Vector3 currentPos = dragBounds.center;
            currentSize.y += 10f;
            currentPos.y += 5f;
            dragBounds.size = currentSize;
            dragBounds.center = currentPos;
            foreach (GameObject character in characters)
            {
                if (dragBounds.Contains(character.transform.position))
                {
                    _selectedCharacters.Add(character);
                }
            }
        }
    }
    private void HandleSelection()
    {
        Debug.Log("Selected " + _selectedCharacters.Count + " characters.");
    }
}
